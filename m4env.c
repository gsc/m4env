/*
 * NAME
 *     m4env - run m4 with environment variables accessible as macros
 *
 * SYNOPSIS
 *     m4env [OPTIONS] [FILES]
 *
 * DESCRIPTION
 *     Modifies the command line by replacing argv[0] with "m4" and
 *     inserting a "-D" option for each environment variable before
 *     the original argv[1].  Then execs m4 (from the PATH) with
 *     the created command line.
 *
 *     As a result, each environment variable is available for expansion
 *     as a m4 macro with the same name.
 *
 *     E.g,
 *
 *        echo "Your user name is LOGNAME" | m4env
 *
 * AUTHOR
 *     Sergey Poznyakoff
 *
 * LICENSE
 *     "THE BEER-WARE LICENSE" (Revision 42):
 *     <gray@gnu.org> wrote this file.  As long as you retain this notice you
 *     can do whatever you want with this stuff. If we meet some day, and you
 *     think this stuff is worth it, you can buy me a beer in return.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>

extern char **environ;

char *progname;

static void
enomem(void)
{
	fprintf(stderr, "%s: not enough memory\n", progname);
	exit(1);
}

int
main(int argc, char **argv)
{
	int xargc;
	char **xargv;
	int nenv, i, j;

	progname = argv[0];
	for (nenv = 0; environ[nenv]; nenv++)
		;

	if ((unsigned long) nenv + argc + 1 > INT_MAX) {
		fprintf(stderr, "%s: environment size too big\n", progname);
		return 1;
	}

	xargc = argc + nenv + 1;
	xargv = calloc(xargc, sizeof(xargv[0]));
	if (!xargv)
		enomem();

	xargv[0] = "m4";
	for (i = 0; i < nenv; i++) {
		size_t len;
		char *def;

		len = strlen(environ[i]) + 3;
		if ((def = malloc(len)) == NULL)
			enomem();
		xargv[i + 1] = strcat(strcpy(def, "-D"), environ[i]);
	}
	for (j = 1; j <= argc; i++, j++)
		xargv[i + 1] = argv[j];

	execvp(xargv[0], xargv);
	perror("execvp");
	return 2;
}
	
